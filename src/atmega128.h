#include "atmega.h"
#ifndef _ATMEGA_128_
#define _ATMEGA_128_

#define _EEPROM_SIZE_ 4
#define _MEMORY_SIZE_ 4

typedef struct {
  PIN A, B, C, D, E, F;
  uint8_t TIMSK, TIFR, SFIOR, ETIMSK, ETIFR;
} Board;

#endif
