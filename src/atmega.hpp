#pragma once
#include <bitset>

typedef bitset<8> REGISTER

class Board{
  public:
    REGISTER DDRA, PORTA, PINA;
    REGISTER DDRB, PORTB, PINB;
    REGISTER DDRC, PORTC, PINC;
    REGISTER DDRD, PORTD, PIND;
    REGISTER DDRE, PORTE, PINE;
    REGISTER DDRF, PORTF, PINF;
  private:
    Observer()
};
