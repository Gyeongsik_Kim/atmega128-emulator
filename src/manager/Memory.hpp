#pragma once

namespace manager{
  class memory{
    public:
      memory(){
        const auto memory_size = 4 * 1024;
        mem = new char[memory_size];
        memset(mem, 0x00, memory_size);
      }
      ~memory(){
        delete[] mem;
      }
      void* malloc(size_t);
      void* calloc(size_t);
      void* realloc(void*, size_t);
      void free(void*);
    private:
      char* mem;
  };
};
