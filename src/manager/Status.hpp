#pragma once
#include <vector>

namespace manager{
  class Status{
    public:
      Handler(){
        observers = std::vector<PIN*>();
      }
      void notify(char* target, char value);

      void addObserver(PIN*);
      void delObserver(PIN*);

      ~Handler(){
        observers.clear();
        delete observers;
      }
    private:
      std::vector<PIN*> observers;
  };
};
