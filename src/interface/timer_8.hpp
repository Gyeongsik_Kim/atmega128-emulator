//8bit 타이머 헤더

#ifndef __TIMER_8_HPP__
#define __TIMER_8_HPP__
#endif

typedef struct {
  uint8_t TCCRx, TCNTx, OCRx, ASSR;
} Timer8;
