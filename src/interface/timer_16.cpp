//16비트 타이머 헤더

#ifndef __TIMER_16_HPP__
#define __TIMER_16_HPP__
#endif

typedef struct {
  uint8_t TCCRxA, TCCRxB, TCCRxC;
  uint16_t TCNTx, OCRxA, OCRxB, OCRxC, ICRx; 
} Timer16;
