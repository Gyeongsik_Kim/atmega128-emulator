#ifndef __PIN_HPP__
#define __PIN_HPP__
#endif

namespace interface{
  class Pin : {
    private:
      int value;
    public:
      Pin(){
        static int _value;
        value = _value++;
      }

      int getPinInfo(){
        return this->value;
      }

  };
};
