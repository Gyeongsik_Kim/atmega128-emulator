#ifndef __PIN_INTERFACE_HPP__
#define __PIN_INTERFACE_HPP__
#endif

namespace interface{
  typedef enum{CHANGE_DIRECTION, CHANGE_VALUE, READ_VALUE} EventType;
  typedef enum{INTERRUPT, PIN, USER} SenderType;
  typedef struct {
    EventType type;

    typedef union {
      Pin pin;
    } sender;
    void (*callback)(uint8_t, uint8_t);
  } Event;

  class PinInterface : {
    private:
    public:
      int getPinInfo();
      int notify(Event* event);
  };
};
